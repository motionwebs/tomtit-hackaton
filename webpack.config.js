'use strict';

const UglifyJsPlugin = require('uglifyjs-webpack-plugin'),
      MinifyPlugin = require("babel-minify-webpack-plugin");

module.exports = {
    target: 'web',
    mode: 'development',

    entry: './index.js',
    output: {
        filename: "bundle.js",
        path: __dirname + '/public'
    },

    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: [
                    {loader: 'babel-loader'}
                ]
            },
            {
                test: /\.less$/,
                exclude: /node_modules/,
                use: [
                    {loader: 'style-loader'},
                    {loader: 'css-loader'},
                    {loader: 'less-loader'}
                ]
            }
        ]
    },
    devServer: {
        port: 3000,
        host: '127.0.0.1',
        watchOptions: {
            pull: true,
        },
        publicPath: '/public/',
        compress: true
    },
    optimization: {
        minimizer: [new UglifyJsPlugin({
            test: /\.js(\?.*)?$/i
        })]
    },
    plugins: [
        new MinifyPlugin({}, {
            test:/\.js($|\?)/i
        })
    ]
}