import {} from './App.less'
import Player from './components/Player'
// Importing styles
import * as Consts from './components/consts'
import Map from './components/Map'
import Voice from './components/Voice'
import RenderPlayerOpts from './components/RenderPlayerOpts'

let Modal =  {
    prefix: 'data-modal',

    close(){
        document.getElementById("modalll").setAttribute("style", "opacity: 0;z-index:0;transition: 0.5s;");
        document.getElementById("inn").setAttribute("style", "opacity: 0;z-index:0;transition: 0.5s;");
    },
    openModal_EndGame() {
        window.Close = this.close
        window.CloseOverflow = this.closeOverflow
        document.body.innerHTML += `
            <div id = "modalll" class = "inner">
                <div class = "modal_w">
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <button><img class = "all"src="images/shop.png"></button>
                    <br>
                    <button onclick="location.reload()"><img class = "all"src="images/play.png"></button>
                    <br>
                    <button><img class = "all"src="images/remove_ads.png"></button>
                    <br>
                    <button><img class = "all"src="images/leaderboard.png"></button>
                </div> 
            </div>
        `;

        //call window
        // document.getElementById("modal").setAttribute("style", "opacity: 1;z-index: 3;transition: 0.5s;");
        document.addEventListener("keydown", event =>{
            if(event.keyCode == 27){
                document.getElementById("modalll").setAttribute("style", "opacity: 0;z-index:0;transition: 0.5s;");
            }
        });
    },

    openModal_Shop() {
        document.write(
            '<div id = "inn"class = "innerr">' +
            '<div class = "modal_www">' +
            '<br>'+ '<br>'+ '<br>'+
            '<br>'+ '<br>'+ '<br>'+ '<br>'+ '<br>'+ '<br>'+ '<br>'+
            '<div class = "squ">'+
            '<img class = "all"src="images/baton.png">' +
            '</div>'+
            '<div class = "squ">'+
            '<img class = "all"src="images/fist.png">' +
            '</div>'+
            '<div class = "squ">'+
            '<img class = "all"src="images/spear.png">' +
            '</div>'+
            '<br>'+
            '<br>'+
            '<br>'+
            '<br>'+
            '<br>'+
            '<br>'+
            '<div class = "squ">'+
            '<img class = "all"src="images/axe.png">' +
            '</div>'+
            '<div class = "squ">'+
            '<img class = "all"src="images/stick.png">' +
            '</div>'+
            '<div class = "squ">'+
            '</div>'+
            '<img style = "width: 150px;float:left;margin-left: 40px;"class = "all"src="images/arrows.png">' +
            '<div style = "margin-left: 50px;"class = "squ">'+
            '<img style = "width:100%;margin-top:10px;"class = "all"src="images/coin.png">' +
            '</div>'+
            '<br>'+
            '<br>'+
            '<br>'+
            '<br>'+
            '<br>'+
            '<br>'+
            '<div class = "squ">'+
            '</div>'+
            '<div class = "squ">'+
            '</div>'+
            '<div class = "squ">'+
            '</div>'+
            '</div>'+
            '</div>'
        );
        document.addEventListener("keydown", event =>{
            if(event.keyCode == 27){
            document.getElementById("inn").setAttribute("style", "opacity: 0;z-index:0;transition: 0.5s;");
        }

    });
    },
    closeModal_Shop(){
        document.getElementById("inn").setAttribute("style", "opacity: 0;z-index:0;transition: 0.5s;");
    }

}

const w = Consts.w,
      h = Consts.h;

const pjs = Consts.pjs
// Главные константы
const Game = pjs.game
const key = pjs.keyControl.initKeyControl()
const System = pjs.system
const Camera = pjs.camera
const OOP = pjs.OOP
const tiles = pjs.tiles
const Mouse = pjs.mouseControl
const math = pjs.math
const vector = pjs.vector
const point = vector.point

Mouse.initMouseControl()

window.Camera = Camera
window.pjs = pjs
window.Vector = vector
window.Moneys = []
window.Modal = Modal

// Инициализация PJS AUDIO
Voice.init(pjs.wAudio.newAudio)


const initMap = Map.init(Game,OOP).setMap()
// Изменение заголовка на странице
System.setTitle('TomTTIT Game')
// Инициализация игры в полный экран
System.initFullPage()
// Получить Fps
System.initFPSCheck()

// Инициализация игрока
let PlayerInit = Player.init(Game,tiles, Voice, vector)
window.Player = PlayerInit
window.Player.userData = {
    health: 6,
    kills: 0,
    score: 0
}

RenderPlayerOpts.render()

// Изменение заголовка на странице
System.setTitle('TomTTIT Game')
// Инициализация игры в полный экран
System.initFullPage()
// Получить Fps
System.initFPSCheck()

Map.createEnemys()
// инициализация главного цикла
let counterCamera = 0
let isCameraFast = false
const PlayerObject = PlayerInit.object

setTimeout(() => { isCameraFast = true }, 2000)
Game.newLoop('game', function () {
    // кординаты мыши
    const MousePosition = Mouse.getPosition()

    if(!isCameraFast) counterCamera += 3
    else counterCamera += 6
    Camera.setPosition(point(counterCamera, 0))
    // действие с главным объектом игрока
    PlayerInit
            .draw()
            .watchWalls(Map.walls, OOP, point)
            .checkDeath()
            .fight(Mouse.isPress('LEFT'))
            .drawFights(MousePosition, point)
            .killEnemy(Map.enemys)
    // Проверка - игрок находится в камере или нет
    if(!PlayerObject.isInCameraStatic()) {
        Game.stop()
        Voice.death()
    }
    // действия с объктом PointJS
    PlayerObject
        .rotate(MousePosition)
    PlayerObject
        .moveTimeC(MousePosition,100)
    // Убитие игрока
    OOP.forArr(Map.enemys, enemy => {
        if(enemy.isStaticIntersect( Player.object.getStaticBox() )) Player.userData.health--
    })
    // Действия с картой игры
    initMap
        .drawMap()
        .drawEnemys()
        .addNewCols(PlayerInit.object.getPosition())
        .drawEnemys()
        .enemyAttack()
        .drawMoney()
        .getMoney()
        .checkScore()
})
// Инициализация и старк игрового цикла
Game.setLoop('game')
// Стартуем цикл
Game.startLoop('game')
// Добавляем фоновую музыку
pjs.wAudio.newAudio('sounds/loop_sound.mp3', 0.05).play()
// Наччинаем игру
Game.start()
