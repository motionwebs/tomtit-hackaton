export default class Sound {
    constructor (sound, volume) {
        this.sound = new Audio()
        this.sound.src = sound
        this.sound.volume = volume
    }
    play () {
        this.sound.play()
    }
}