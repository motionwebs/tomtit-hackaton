import * as MobOptions from './Mob'
export default {
    OOP: null,
    Game: null,

    // Строковое представление карты
    mapString: [
        '              1',
        '              1',
        '              1',
        '              1',
        '              1',
        '              1',
        '              1',
        '              1',
        '              1',
        '              1',
        '               ',
        '               ',
        '               ',
        '               ',
        '               ',
        '               ',
        '               ',
        '              1',
        '              1',
        '              1',
        '              1',
        '              1'
    ],
    wallCounter: 0,
    walls: [],
    enemys: [],
    money: 0,
    enemyProps: MobOptions,
    // Каждое свойство в объекте означает свой метод
    mainObject: null,
    // Символы карты
    symbols: {
        '0': {
            path: 'images/location/levels-blocks/2 lvl/rock 2 lvl.png',
            w: 150,
            h: 60
        },
        '*': {
            path: 'images/location/levels-blocks/1 lvl/rock 1 lvl.png',
            w: 50,
            h: 50
        },
        '1': {
            path: 'images/b1.png',
            w: 50,
            h: 50
        },
        'b': {
            path: 'images/b1.png',
            w: 80,
            h: 80
        },
        'g': {
            path: 'images/location/levels-blocks/2 lvl/grass 2 lvl.png',
            w: 100,
            h: 50
        }
    },
    blockW: 100,
    blockH: 50,
    settingScore: false,
    // Символы карты
    // Отрисовка символов
    setMap () {
        this.OOP.forArr(this.mapString.reverse(), (string, Y) => {
            this.OOP.forArr(string, (symbol, X) => {
                if(symbol === '' || symbol === ' ') return
                let sym = this.symbols[symbol]
                let patternObject = {
                    x: sym.w * X,
                    y: (window.innerHeight - sym.h) - Y * sym.h,
                    file: sym.path,
                    w: sym.w,
                    h: sym.h
                }
                if(symbol !== 'M') {
                    this.walls.push(
                        this.mainObject(patternObject)
                    )
                } else {
                    this.enemys.push(
                        this.mainObject(patternObject)
                    )
                }
            })
        })
        return this
    },
    createEnemys () {
        setInterval(() => {
            let last = this.walls.filter(wall => {
                return wall.file === 'images/b1.png'
            }).map(item => {
                return item.x
            })
            last = Math.max.apply(null, last)
            this.enemys.push(
                this.Game.newAnimationObject({
                    delay: 5,
                    animation: pjs.tiles.newImage('images/bird_blue.png').getAnimation(-15,0,130,130,8),
                    x: last - 80,
                    y: Math.random() * window.innerHeight,
                    w: 50,
                    h: 50
                })
            )
        }, 2000)
        return this
    },
    addNewCols (cameraPos) {
        let { x } = cameraPos
        let last = this.walls.filter(wall => {
            return wall.file === 'images/b1.png'
        }).map(item => {
            return item.x
        })
        let maxLastXPosition = Math.max.apply(null,last)
        if(x >= maxLastXPosition && maxLastXPosition !== -Infinity) {
            for(let k = 1; k < 6; k++) {
                let randTop = Math.floor((Math.random() + 2) * 2 + Math.floor(Math.random() * 5))
                let randBottom = Math.floor((Math.random() + 2) * 2 + Math.floor(Math.random() * 5))
                let xPos = x + 600 * k
                for(let i = 0; i < randTop; i++) {
                    this.walls.push(
                        this.Game.newImageObject({
                            file: 'images/b1.png',
                            w: 50,
                            h: 50,
                            x: xPos,
                            y: 50 * i
                        })
                    )
                }
                for(let j = 0; j < randBottom; j++) {
                    this.walls.push(
                        this.Game.newImageObject({
                            file: 'images/b1.png',
                            w: 50,
                            h: 50,
                            x: xPos,
                            y: window.innerHeight - (50 * j)
                        })
                    )
                }
                let centerPosition = ((50 * randTop + ((randTop * 25))) + (50 * randBottom + (randBottom * 25))) / 2
                Moneys.push(
                this.Game.newAnimationObject({
                    x: xPos,
                    y: centerPosition,
                    w: 30,
                    h: 30,
                    delay: .1,
                    animation: pjs.tiles.newImage('images/coin.png').getAnimation(15,0,120,120,8),
                }))
            }

            return this
        }
        return this
    },
    drawEnemys () {
        this.OOP.drawArr(this.enemys)
        return this
    },
    drawMoney () {
        this.OOP.drawArr(Moneys)
        return this
    },
    getMoney () {
        this.OOP.forArr(Moneys, (money, i) => {
            if(Player.object.isStaticIntersect(money)) {
                this.money++
                this.saveMoney()
                Moneys.splice(i,1)
            }
        })
        return this
    },
    saveMoney () {
        localStorage.setItem('money', this.money)
    },
    checkScore () {
        let firstWall = this.walls[this.wallCounter]
        let { x } = Player.object
        let counter = 0
        if(x > firstWall.x && !this.settingScore) {
            counter++
            this.wallCounter++
            if(counter === 1) {
                if(this.wallCounter - this.wallCounter++ === 0) {
                    this.settingScore = true
                    Player.userData.score++
                    this.settingScore = false
                    return
                }
            }
        }

        return
    },
    enemyAttack () {
        let counter = 0
        this.OOP.forArr(this.enemys, (enemy, i) => {
            if( enemy.getDistanceC( Player.object.getPosition(1) ) <= 1000 ) {
                enemy.moveTimeC( Player.object.getPosition(), 80 )
                enemy.rotate( Player.object.getPosition(1) )
                enemy.moveAngle(1)
            }
            this.OOP.forArr(this.walls, wall => {
                if(wall.isStaticIntersect(enemy.getStaticBox(5,0))) {
                    enemy.x = wall.x - wall.w
                } else if (wall.isStaticIntersect(enemy.getStaticBox(-5,0))) {
                    enemy.x = wall.x + wall.w
                } else if(wall.isStaticIntersect(enemy.getStaticBox(0,-5))) {
                    enemy.y = wall.y + wall.h
                } else if(wall.isStaticIntersect(enemy.getStaticBox(0,5))) {
                    enemy.y = wall.y - wall.h
                }
            })
        })
        return this
    },
    drawMap () {
        this.OOP.drawArr(this.walls)
        return this
    },
    // Инициализация карты
    init (Game, OOP) {
        this.money = (localStorage.getItem('money')) ? localStorage.getItem('money') : (0, localStorage.setItem('money',0))
        this.Game = Game
        this.OOP = OOP
        this.mainObject = this.Game.newImageObject
        return this
    }
}