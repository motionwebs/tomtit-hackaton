
export default {
    Game: null,
    object: null,
    walls: [],
    tiles: null,
    vector: null,
    vy: .1,
    vx: 0,
    OOP: null,

    bullet: {
        speed: 30
    },

    fights: [],
    voice: null,
    checkBulletCollision (bull, index) {
        let { x } = Camera.getPosition()
        if(bull.x <= 0 + x ||
            bull.x >= x + window.innerWidth ||
            bull.y <= 0 ||
            bull.y >= window.innerHeight) {
            this.fights.splice(index,1)
        }
    },
    createObject() {
        this.object = this.Game.newAnimationObject({
            x : 300,
            y : window.innerHeight / 2,
            w : 110,
            h : 100,
            delay: 2,
            animation : this.tiles.newImage("images/burd.png").getAnimation(-20, 0, 110, 100, 14),
        })
        return this
    },
    draw() {
        this.object.draw()
        return this
    },
    watchWalls (walls, OOP, point) {
        this.OOP = OOP
        this.walls = walls

        const P = Player.object
        let HealthStart = Player.userData.health
        OOP.forArr(this.walls, (wall, i) => {
            if(wall.isStaticIntersect( P.getStaticBox() )) {
                let Health= Player.userData.health
                Health--

                if(P.x  <= wall.x + wall.w && wall.x <= P.x) {
                    P.x = wall.x + wall.w + 1
                }
                if((P.x + P.w) + 2 >= wall.x &&
                    P.x + P.w <= wall.x + wall.w) {
                    P.x = wall.x - P.w
                }

                setTimeout(() => {
                    Player.userData.health = Health
                }, 400)
                return
            }
        })
        return this
    },
    fight (isDownLeft) {
        if(isDownLeft) {
            this.voice.fight()
            this.fights.push(
                this.Game.newRectObject({
                    x: this.object.x + this.object.w / 2,
                    y: this.object.y + this.object.h / 2,
                    w: 10,
                    h: 5,
                    fillColor: '#fff',
                    userData: {
                        isFight: false
                    }
                })
            )
        }
        return this
    },
    drawFights (mousePosition, point) {
        this.OOP.forArr(this.fights, (bull, i) => {
            if(bull.isFight) {
                bull.rotate( bull.rotateAngle )
                bull.moveAngle( this.bullet.speed, bull.angleMove )
                bull.draw()
                this.checkBulletCollision(bull,i)
            }
            else {
                bull.rotateAngle = mousePosition
                bull.angleMove = this.object.getAngle()
                bull.isFight = true
            }
        })
        return this
    },
    checkDeath () {
        if(this.object.y >= window.innerHeight - this.object.h || this.object.y <= 0) {
            this.Game.stop()
            this.voice.death()
        }
        if(Player.userData.health <= 0) {
            this.Game.stop()
            this.voice.death()
        }
        return this
    },
    killEnemy (enemys) {
        this.OOP.forArr(this.fights, (fight, fightIndex) => {
            this.OOP.forArr(enemys, (enemy, enemyIndex) => {
                if( enemy.isStaticIntersect( fight.getStaticBox() ) ) {
                    let { x, y } = enemy.getPosition()
                    Moneys.push(
                        this.Game.newAnimationObject({
                            x: x - 2.5 + enemy.w / 2 ,
                            y: y - 2.5 + enemy.h/2,
                            delay: .1,
                            animation: pjs.tiles.newImage('images/coin.png').getAnimation(15,0,120,120,8),
                            w: 30, h: 30
                        })
                    )
                    enemys.splice(enemyIndex,1)
                    this.fights.splice(fightIndex,1)
                    Player.userData.kills++
                    return
                }
            })
        })
    },
    init (Game, tiles, voice, vector, Camera) {
        this.Game = Game
        this.tiles = tiles
        this.voice = voice
        this.vector = vector
        this.createObject()
        return this
    }
}