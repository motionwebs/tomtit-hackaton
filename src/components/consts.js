import PointJS from '../libs/pointjs'

const w = window.innerWidth, // ширина экрана
    h = window.innerHeight, // высота экрана
    pjs = new PointJS(w,h); // главный объект PointJS

export { w, h, pjs }