export default {
    containers: {
        healthContainer: document.querySelector('.game-panel__health-count'),
        scoreContainer: document.querySelector('.game-panel__score-count b'),
        killsContainer: document.querySelector('.game-panel__kills-count b'),
        timeContainer: document.querySelector('.game-panel__time-count'),
        moneyContainer: document.querySelector('.game-panel__money-count b')
    },
    kills: 0,
    health: 0,
    healthString: [],
    score: 0,
    time: {
        minutes: 0,
        seconds: 0
    },
    timeLoop: 1000,
    startLoop () {
        setInterval(() => {
            this.health = Player.userData.health
            this.score = Player.userData.score
            this.kills = Player.userData.kills
            this.renderMoney()
            this.startTime()
            this.setData(this.containers.timeContainer, this.convertTime())
            this.setData(this.containers.killsContainer, this.kills)
            this.setData(this.containers.scoreContainer, this.score)
            this.setData(this.containers.healthContainer, this.setHealth())
        }, this.timeLoop)
    },
    renderMoney () {
        this.setData(this.containers.moneyContainer, localStorage.getItem('money'))
    },
    setHealth () {
        let str = ''
        for(let i = 0; i < this.health; i++) {
            str += '<img src="images/heart.png" alt="">'
        }
        return str
    },
    setData (container, data) {
        container.innerHTML = data
    },
    convertTime () {
        let mins = this.time.minutes,
            secs = this.time.seconds

        if(mins < 10) mins = '0' + mins
        if(secs < 10) secs = '0' + secs

        return mins + ':' + secs
    },
    startTime () {
        this.time.seconds++

        if(this.time.seconds >= 60) {
            this.time.seconds = 0
            this.time.minutes++
        }
    },
    render () {
        this.startLoop()
    }
}