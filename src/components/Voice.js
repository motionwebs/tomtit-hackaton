export default {
    sound: null,
    playSound(path) {
        new this.sound(path,.5).play()
    },
    death () {
        this.playSound('sounds/uii.mp3')
        Modal.openModal_EndGame()
    },
    collision () {
        this.playSound('sounds/death.wav')
    },
    fight () {
        this.playSound('sounds/plevok.mp3')
    },
    init (pjsAudio) {
        this.sound = pjsAudio
    }
}